import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 导入qiankun.js
import { registerMicroApps, setDefaultMountApp, start } from 'qiankun'
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')

// 注册子应用
registerMicroApps([
  {
    name: 'vue app', // 子应用名称
    entry: '//localhost:7101', // 子应用入口
    container: '#container', // 子应用所在容器
    activeRule: '/childOne' // 子应用触发规则（路径）
  },
  {
    name: 'vue app2', // 子应用名称
    entry: '//localhost:7102', // 子应用入口
    container: '#container', // 子应用所在容器
    activeRule: '/childTwo' // 子应用触发规则（路径）
  }
])

// 启动默认应用
setDefaultMountApp('/')

// 开启服务
start()
