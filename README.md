# Qiankun 微前端框架初探（Vue2）

[文章说明](http://xiaocjee.gitee.io/guide/web/qiankun/)

> 使用 `qiankun` 框架结合 `Vue2` 制作的一个简单 **demo**

:smiley:  

## 说明

**vue-main** 为主应用  
*vue-childOne*、*vue-childTwo* 为子应用

![qiankun](https://xiaocjee.gitee.io/assets/img/qiankun-02.ae4acb47.jpg)

:+1:
